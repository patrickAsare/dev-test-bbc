import React, { Component } from 'react';
import axios from 'axios';
import Header from './Header';
import Title from './Title';
import FileList from './FileList';
import FileTypes from './FileTypes';
import '../css/App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: [],
      fileTypeData: [],
      fileData: [],
      fileDataFilterList: [],
      requestErrorsMade: 0,
      userRequestLimit: 10
    };

    this.updateFileFilterList = this.updateFileFilterList.bind(this);
  }

  componentDidMount() {
    this.getData('http://localhost:3001/users', 'userData');
    this.getData('http://localhost:3001/types', 'fileTypeData');
    this.getData('http://localhost:3001/files', 'fileData');
  }

  getData(url, dataType) {
    //user data endpoint is flakey so request a few times
    let catchErrorFunc = dataType === 'userData' ? this.requestAgainOnError.bind(this) : this.displayMsgOnError ;

    axios({
      method:'get',
      url: url,
      timeout: 5000
    }).then(this.loaded.bind(this, dataType)).catch(catchErrorFunc);
  }

  loaded(dataType, res) {
    let newState = {};
    newState[dataType] = res.data;
    this.setState(newState);

    if (dataType === 'fileData') {
      this.updateFileFilterList(dataType);
    }
  }

  updateFileFilterList() {
    let newState = {};
    if (this.state.fileDataFilterList.length === 5) {
      newState.fileDataFilterList = this.state.fileData.slice(0, this.state.fileData.length);
    } else {
      newState.fileDataFilterList = this.state.fileData.slice(0, 5);
    }
    this.setState(newState);
  }

  requestAgainOnError(err) {
    //should really check specifically for timeouts and 500s because this will catch all errors
    this.setState({ requestErrorsMade: this.state.requestErrorsMade + 1 });
    console.log(err, this.state, 'Request Errors Made: ', this.state.requestErrorsMade, ' Request Limit: ', this.state.userRequestLimit);
    if (this.state.requestErrorsMade < this.state.userRequestLimit) {
      this.getData('http://localhost:3001/users', 'userData');
    }
  }

  displayMsgOnError(err) {
    console.log(err);
  }

  getFileType(file) {
    return this.fileTypeData.find(fileType => fileType.id === file.type);
  }

  getUserWhoModifiedFile(file) {
    return this.userData.find(user => user.id === file.modifiedBy);
  }

  render() {
    return (
      <div className="App">
        <Header />
        <div className="container">

          <div className="row">
            <div className="col-md-8">
              <Title userData={this.state.userData} />
              <FileList
                userData={this.state.userData}
                fileTypeData={this.state.fileTypeData}
                fileData={this.state.fileDataFilterList}
                showAllFiles={this.showAllFiles}
                getFileType={this.getFileType}
                getUserWhoModifiedFile={this.getUserWhoModifiedFile}
                updateFileFilterList={this.updateFileFilterList}
              />
            </div>

            <div className="col-md-4">
              <FileTypes fileTypeData={this.state.fileTypeData} />
            </div>
          </div>

        </div>
      </div>
    );
  }
}

export default App;
