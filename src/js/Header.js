import React from 'react';
import logo from '../img/logo.png';

const Header = () =>
  <div className="HeaderComponent">
    <img src={logo} alt="Logo" /> <h1>iSite Dashboard</h1>
  </div>

export default Header;
