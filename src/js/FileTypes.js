import React from 'react';

const FileTypes = (props) =>
  <div className="FileTypesComponent">
    <header>File Types</header>
    <ol>
    {
      props.fileTypeData.map((fileType) =>
        <li key={fileType.name}>
          <p title={fileType.description}>{fileType.name} ({fileType.documentsCount})</p>
        </li>
      )
    }
    </ol>
  </div>

export default FileTypes;
