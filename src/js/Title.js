import React from 'react';
import users from '../img/users.png';

const Title = (props) =>
  <div className="TitleComponent">
    <img src={users} alt="Users Logo" />
    <div className="TitleComponent__content">
      <h2>Programmes - Dr Who</h2>
      <p>This project has {props.userData.length} users</p>
    </div>
  </div>

export default Title;
