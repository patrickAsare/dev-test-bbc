import React from 'react';
import FileListItem from './FileListItem';

const FileList = (props) =>
  <div className="FileListComponent">
    <h3>Latest Content</h3>
    <ul>
    {
      props.fileData.map((file) =>
        <FileListItem
          file={file} key={file.title}
          fileTypeData={props.fileTypeData}
          userData={props.userData}
          getFileType={props.getFileType}
          getUserWhoModifiedFile={props.getUserWhoModifiedFile}
        />
      )
    }
    </ul>
    <button onClick={props.updateFileFilterList}>Show All</button>
  </div>


export default FileList;
