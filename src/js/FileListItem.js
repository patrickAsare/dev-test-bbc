import React from 'react';
import moment from 'moment';

const FileListItem = (props) =>
  <li>
    <div className="FileListItemComponent">
      <div><a href={props.file.uri} alt={props.file.title} title={props.file.title}>{props.file.title}</a></div>
      <div> Type: {props.file.type}</div>
      <div> Icon: {props.getFileType(props.file).colourId}</div>
      {props.getUserWhoModifiedFile(props.file) &&
        <div className=""> modified by {props.getUserWhoModifiedFile(props.file).givenName} {props.getUserWhoModifiedFile(props.file).familyName} {moment(props.file.modifiedDateTime).fromNow()} - {props.file.status}</div>
      }
    </div>
  </li>

export default FileListItem;
