import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, mount } from 'enzyme';
import FileList from '../js/FileList';
import userData from './fixtures/UserData';
import fileTypeData from './fixtures/FileTypeData';
import fileData from './fixtures/FileData';

//Mocks
const getUserWhoModifiedFile = jest.fn();
const getFileType = jest.fn();
const updateFileFilterList = jest.fn();

getUserWhoModifiedFile.mockReturnValue({
  "id": 1,
  "givenName": "Peter",
  "familyName": "Capaldi"
});

getFileType.mockReturnValue({
  "creationDateTime": "2016-08-17T13:07:19.800Z",
  "id": "article",
  "documentsCount": 5,
  "description": "Articles about the programme",
  "name": "Article Page",
  "colourId": "golden"
});

it('renders without crashing', () => {
  const wrapper = shallow(<FileList
    userData={userData}
    fileTypeData={fileTypeData}
    fileData={fileData}
    updateFileFilterList={updateFileFilterList}
    getFileType={getFileType}
    getUserWhoModifiedFile={getUserWhoModifiedFile}
  />);
});

it('renders a header', () => {
  const wrapper = shallow(<FileList
    userData={userData}
    fileTypeData={fileTypeData}
    fileData={fileData}
    updateFileFilterList={updateFileFilterList}
    getFileType={getFileType}
    getUserWhoModifiedFile={getUserWhoModifiedFile}
  />);
  const text = <h3>Latest Content</h3>;
  expect(wrapper.contains(text)).toEqual(true);
});

it('renders 20 records', () => {
  const wrapper = mount(<FileList
    userData={userData}
    fileTypeData={fileTypeData}
    fileData={fileData}
    updateFileFilterList={updateFileFilterList}
    getFileType={getFileType}
    getUserWhoModifiedFile={getUserWhoModifiedFile}
  />);

  expect(wrapper.find('ul').children().length).toBe(fileData.length);
});
