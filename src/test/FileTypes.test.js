import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, mount } from 'enzyme';
import FileTypes from '../js/FileTypes';
import fileTypeData from './fixtures/FileTypeData';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<FileTypes fileTypeData={fileTypeData} />, div);
});

it('renders a header', () => {
  const wrapper = shallow(<FileTypes fileTypeData={fileTypeData} />);
  const text = <header>File Types</header>;
  expect(wrapper.contains(text)).toEqual(true);
});

it('renders the number of article pages', () => {
  const wrapper = shallow(<FileTypes fileTypeData={fileTypeData} />);
  const text = <p title="Articles about the programme">Article Page (5)</p>;
  expect(wrapper.contains(text)).toEqual(true);
});

it('renders the number of profile pages', () => {
  const wrapper = shallow(<FileTypes fileTypeData={fileTypeData} />);
  const text = <p title="Actor/Actress profiles">Profile Page (6)</p>;
  expect(wrapper.contains(text)).toEqual(true);
});
