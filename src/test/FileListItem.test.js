import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, mount } from 'enzyme';
import FileListItem from '../js/FileListItem';
import userData from './fixtures/UserData';
import fileTypeData from './fixtures/FileTypeData';
import fileData from './fixtures/FileData';
import file from './fixtures/File';

//Mocks
const getUserWhoModifiedFile = jest.fn();
const getFileType = jest.fn();

getUserWhoModifiedFile.mockReturnValue({
  "id": 1,
  "givenName": "Peter",
  "familyName": "Capaldi"
});

getFileType.mockReturnValue({
  "creationDateTime": "2016-08-17T13:07:19.800Z",
  "id": "article",
  "documentsCount": 5,
  "description": "Articles about the programme",
  "name": "Article Page",
  "colourId": "golden"
});

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<FileListItem
    file={file} key={file.title}
    fileTypeData={fileTypeData}
    userData={userData}
    getFileType={getFileType}
    getUserWhoModifiedFile={getUserWhoModifiedFile}
  />, div);
});

it('renders a file', () => {
  const wrapper = mount(<FileListItem
    file={file} key={file.title}
    fileTypeData={fileTypeData}
    userData={userData}
    getFileType={getFileType}
    getUserWhoModifiedFile={getUserWhoModifiedFile}
  />);
  expect(wrapper.text()).toEqual('Bedi ewosawal. Type: article Icon: golden modified by Peter Capaldi 2 years ago - In progress');
});
