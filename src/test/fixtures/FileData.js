const fileData = [
  {
    "creationDateTime": "2015-08-06T18:20:59.216Z",
    "status": "In progress",
    "modifiedBy": 6,
    "type": "article",
    "uri": "/project/test/content/9eabaf0e-b511-5e4e-87bc-c23c2cb3d27d",
    "version": 1,
    "id": "9eabaf0e-b511-5e4e-87bc-c23c2cb3d27d",
    "fileId": "Bedi-ewosawal.",
    "scheduled": false,
    "title": "Bedi ewosawal.",
    "createdBy": 1,
    "modifiedDateTime": "2015-08-07T01:20:59.216Z",
    "live": true,
    "popularity": 3
  },
  {
    "creationDateTime": "2015-05-13T16:38:19.923Z",
    "status": "For review",
    "modifiedBy": 7,
    "type": "profile",
    "uri": "/project/test/content/1e2eeca6-fd6b-5aef-b209-1e7b72dddd67",
    "version": 1,
    "id": "1e2eeca6-fd6b-5aef-b209-1e7b72dddd67",
    "fileId": "Sa-vow.",
    "scheduled": false,
    "title": "Sa vow.",
    "createdBy": 4,
    "modifiedDateTime": "2015-05-14T00:38:19.923Z",
    "live": true,
    "popularity": false
  },
  {
    "creationDateTime": "2015-07-02T06:01:15.022Z",
    "status": "Approved",
    "modifiedBy": 4,
    "type": "article",
    "uri": "/project/test/content/0e918b56-e163-5ad1-bad8-a15339e2acc4",
    "version": 1,
    "id": "0e918b56-e163-5ad1-bad8-a15339e2acc4",
    "fileId": "Tugiro-gu.",
    "scheduled": false,
    "title": "Tugiro gu.",
    "createdBy": 8,
    "modifiedDateTime": "2015-07-02T07:01:15.022Z",
    "live": true,
    "popularity": false
  },
  {
    "creationDateTime": "2015-03-10T21:04:18.130Z",
    "status": "In progress",
    "modifiedBy": 4,
    "type": "article",
    "uri": "/project/test/content/3609cad0-a917-5a43-bd29-bb34296e02b1",
    "version": 2,
    "id": "3609cad0-a917-5a43-bd29-bb34296e02b1",
    "fileId": "Mohcozum-usomajol.",
    "scheduled": false,
    "title": "Mohcozum usomajol.",
    "createdBy": 6,
    "modifiedDateTime": "2015-03-11T05:04:18.130Z",
    "live": true,
    "popularity": 2
  },
  {
    "creationDateTime": "2015-03-09T07:31:13.346Z",
    "status": "Scheduled",
    "modifiedBy": 6,
    "type": "profile",
    "uri": "/project/test/content/0791a445-a649-5264-82e8-1e0a1413b5a4",
    "version": 1,
    "id": "0791a445-a649-5264-82e8-1e0a1413b5a4",
    "fileId": "Harcawfo-gozuwsi.",
    "scheduled": true,
    "title": "Harcawfo gozuwsi.",
    "createdBy": 5,
    "modifiedDateTime": "2015-03-09T11:31:13.346Z",
    "live": false,
    "popularity": 1
  },
  {
    "creationDateTime": "2015-03-08T10:11:08.081Z",
    "status": "Approved",
    "modifiedBy": 5,
    "type": "profile",
    "uri": "/project/test/content/0e01734c-55e4-50d6-a0a3-8ebba29c3eef",
    "version": 3,
    "id": "0e01734c-55e4-50d6-a0a3-8ebba29c3eef",
    "fileId": "Hiroz-le.",
    "scheduled": false,
    "title": "Hiroz le.",
    "createdBy": 8,
    "modifiedDateTime": "2015-03-08T15:11:08.081Z",
    "live": true,
    "popularity": false
  },
  {
    "creationDateTime": "2015-07-30T11:46:13.805Z",
    "status": "For review",
    "modifiedBy": 1,
    "type": "profile",
    "uri": "/project/test/content/4fabcfb0-6fc1-5207-a788-9f22313271e4",
    "version": 1,
    "id": "4fabcfb0-6fc1-5207-a788-9f22313271e4",
    "fileId": "Bo-uz.",
    "scheduled": false,
    "title": "Bo uz.",
    "createdBy": 2,
    "modifiedDateTime": "2015-07-30T17:46:13.805Z",
    "live": true,
    "popularity": 5
  },
  {
    "creationDateTime": "2015-02-02T04:35:05.124Z",
    "status": "Scheduled",
    "modifiedBy": 7,
    "type": "profile",
    "uri": "/project/test/content/b1e8444c-10f9-59a1-8cf6-e990b3d6299a",
    "version": 4,
    "id": "b1e8444c-10f9-59a1-8cf6-e990b3d6299a",
    "fileId": "Pohoigo-pobira.",
    "scheduled": true,
    "title": "Pohoigo pobira.",
    "createdBy": 2,
    "modifiedDateTime": "2015-02-02T06:35:05.124Z",
    "live": true,
    "popularity": false
  },
  {
    "creationDateTime": "2015-02-19T12:13:29.666Z",
    "status": "Scheduled",
    "modifiedBy": 5,
    "type": "profile",
    "uri": "/project/test/content/b56ec9a1-b4ba-5633-bfd3-b9ac68b942ba",
    "version": 4,
    "id": "b56ec9a1-b4ba-5633-bfd3-b9ac68b942ba",
    "fileId": "Tep-imuekesa.",
    "scheduled": true,
    "title": "Tep imuekesa.",
    "createdBy": 7,
    "modifiedDateTime": "2015-02-19T16:13:29.666Z",
    "live": true,
    "popularity": 7
  },
  {
    "creationDateTime": "2015-03-16T14:18:16.568Z",
    "status": "In progress",
    "modifiedBy": 6,
    "type": "profile",
    "uri": "/project/test/content/fc307b55-30a8-52d8-9dfd-a99a660181bf",
    "version": 2,
    "id": "fc307b55-30a8-52d8-9dfd-a99a660181bf",
    "fileId": "Wedesev-okohoh.",
    "scheduled": false,
    "title": "Wedesev okohoh.",
    "createdBy": 5,
    "modifiedDateTime": "2015-03-16T22:18:16.568Z",
    "live": false,
    "popularity": false
  },
  {
    "creationDateTime": "2015-08-06T03:43:52.522Z",
    "status": "Approved",
    "modifiedBy": 7,
    "type": "article",
    "uri": "/project/test/content/11c8841f-5260-59e5-a611-0067700e4397",
    "version": 2,
    "id": "11c8841f-5260-59e5-a611-0067700e4397",
    "fileId": "Cuhof-felcam.",
    "scheduled": false,
    "title": "Cuhof felcam.",
    "createdBy": 4,
    "modifiedDateTime": "2015-08-06T10:43:52.522Z",
    "live": true,
    "popularity": 4
  },
  {
    "creationDateTime": "2015-08-02T03:43:10.991Z",
    "status": "For review",
    "modifiedBy": 3,
    "type": "profile",
    "uri": "/project/test/content/0d7feb31-5608-534b-947b-4083ba1eda75",
    "version": 1,
    "id": "0d7feb31-5608-534b-947b-4083ba1eda75",
    "fileId": "Guvzousa-ejusemi.",
    "scheduled": false,
    "title": "Guvzousa ejusemi.",
    "createdBy": 6,
    "modifiedDateTime": "2015-08-02T09:43:10.991Z",
    "live": false,
    "popularity": false
  },
  {
    "creationDateTime": "2015-06-03T22:49:52.138Z",
    "status": "For review",
    "modifiedBy": 8,
    "type": "profile",
    "uri": "/project/test/content/ccd3bd54-fd89-5307-a828-bd1feef12e89",
    "version": 2,
    "id": "ccd3bd54-fd89-5307-a828-bd1feef12e89",
    "fileId": "Luhca-tedemusi.",
    "scheduled": false,
    "title": "Luhca tedemusi.",
    "createdBy": 1,
    "modifiedDateTime": "2015-06-04T01:49:52.138Z",
    "live": true,
    "popularity": 6
  },
  {
    "creationDateTime": "2015-09-22T11:32:07.844Z",
    "status": "For review",
    "modifiedBy": 7,
    "type": "article",
    "uri": "/project/test/content/a6b0b21c-e9ba-5181-803c-65176db938b5",
    "version": 3,
    "id": "a6b0b21c-e9ba-5181-803c-65176db938b5",
    "fileId": "Apdod-womo.",
    "scheduled": false,
    "title": "Apdod womo.",
    "createdBy": 5,
    "modifiedDateTime": "2015-09-22T18:32:07.844Z",
    "live": true,
    "popularity": false
  },
  {
    "creationDateTime": "2015-03-30T23:33:12.960Z",
    "status": "Approved",
    "modifiedBy": 5,
    "type": "profile",
    "uri": "/project/test/content/57b0a97f-cbb4-5040-be99-8ae0e75fc67c",
    "version": 3,
    "id": "57b0a97f-cbb4-5040-be99-8ae0e75fc67c",
    "fileId": "Genrukug-fiv.",
    "scheduled": false,
    "title": "Genrukug fiv.",
    "createdBy": 2,
    "modifiedDateTime": "2015-03-31T06:33:12.960Z",
    "live": true,
    "popularity": false
  },
  {
    "creationDateTime": "2015-02-24T04:45:15.173Z",
    "status": "Published",
    "modifiedBy": 7,
    "type": "profile",
    "uri": "/project/test/content/3cbbd957-6b4f-5efa-8b50-a49ef47006e6",
    "version": 2,
    "id": "3cbbd957-6b4f-5efa-8b50-a49ef47006e6",
    "fileId": "Pikmizsob-lensudi.",
    "scheduled": false,
    "title": "Pikmizsob lensudi.",
    "createdBy": 5,
    "modifiedDateTime": "2015-02-24T09:45:15.173Z",
    "live": false,
    "popularity": 10
  },
  {
    "creationDateTime": "2015-11-08T18:52:20.179Z",
    "status": "Approved",
    "modifiedBy": 5,
    "type": "profile",
    "uri": "/project/test/content/71291bb4-6f24-5aed-bd56-683f07a9df9f",
    "version": 4,
    "id": "71291bb4-6f24-5aed-bd56-683f07a9df9f",
    "fileId": "Hejgeg-hofcic.",
    "scheduled": false,
    "title": "Hejgeg hofcic.",
    "createdBy": 2,
    "modifiedDateTime": "2015-11-09T01:52:20.179Z",
    "live": false,
    "popularity": false
  },
  {
    "creationDateTime": "2015-03-21T02:00:54.358Z",
    "status": "For review",
    "modifiedBy": 2,
    "type": "profile",
    "uri": "/project/test/content/a6d6cc98-7cf0-5e5a-8ae7-c1a682cea393",
    "version": 2,
    "id": "a6d6cc98-7cf0-5e5a-8ae7-c1a682cea393",
    "fileId": "Or-vouh.",
    "scheduled": false,
    "title": "Or vouh.",
    "createdBy": 6,
    "modifiedDateTime": "2015-03-21T06:00:54.358Z",
    "live": false,
    "popularity": 9
  },
  {
    "creationDateTime": "2015-02-20T04:56:34.581Z",
    "status": "Scheduled",
    "modifiedBy": 1,
    "type": "profile",
    "uri": "/project/test/content/06f8c2aa-d980-50ad-b22e-60eee163cfe2",
    "version": 2,
    "id": "06f8c2aa-d980-50ad-b22e-60eee163cfe2",
    "fileId": "Ewdowre-do.",
    "scheduled": true,
    "title": "Ewdowre do.",
    "createdBy": 2,
    "modifiedDateTime": "2015-02-20T10:56:34.581Z",
    "live": false,
    "popularity": 8
  },
  {
    "creationDateTime": "2015-01-13T21:39:46.561Z",
    "status": "In progress",
    "modifiedBy": 5,
    "type": "article",
    "uri": "/project/test/content/3a194e26-6b43-56cc-8a0d-7d4c927303d9",
    "version": 4,
    "id": "3a194e26-6b43-56cc-8a0d-7d4c927303d9",
    "fileId": "Rim-anu.",
    "scheduled": false,
    "title": "Rim anu.",
    "createdBy": 4,
    "modifiedDateTime": "2015-01-14T02:39:46.561Z",
    "live": false,
    "popularity": false
  }
];

export default fileData;
