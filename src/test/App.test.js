import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, mount } from 'enzyme';
import App from '../js/App';

it('renders without crashing', () => {
  App.prototype.getData = jest.fn(); //Mock
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});
