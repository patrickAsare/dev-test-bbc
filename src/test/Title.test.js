import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, mount } from 'enzyme';
import Title from '../js/Title';
import userData from './fixtures/UserData';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Title userData={userData} />, div);
});

it('renders a header', () => {
  const wrapper = shallow(<Title userData={userData} />);
  const text = <h2>Programmes - Dr Who</h2>;
  expect(wrapper.contains(text)).toEqual(true);
});

it('renders the amount of users', () => {
  const wrapper = shallow(<Title userData={userData} />);
  const text = <p>This project has 8 users</p>;
  expect(wrapper.contains(text)).toEqual(true);
});
