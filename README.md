### What is this repository for? ###

* Code Test

### How do I get set up? ###

1. Checkout repo `git clone https://bitbucket.org/patrickAsare/dev-test-bbc`

##### Api
This App requires a local api

2. cd into `WebDev_Code_Test`
3. cd into `api`
4. run `npm install`
5. run `node index.js`

Ensure api running on `http://localhost:3001/`

##### App

6. cd into `dev-test-bbc`
7. run `npm install`
8. run `npm start` to start app
9. visit `http://localhost:3000/`
10. to test run `npm test`

### Notes / Improvements ###
* Only had a day or so due to very bad illness
* Need to test App component methods and async api calls
* One stateful component
* Could do with a bit of sass
* Could refactor loads
* First time using react outside of BBC Morph

### Support ###
* node v8.2.1
* 5.3.0
* latest Chrome

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
